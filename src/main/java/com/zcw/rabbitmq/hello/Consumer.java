package com.zcw.rabbitmq.hello;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者类
 */
public class Consumer {

    /**
     * 获取消费者，并消费消息
     * @param queueNAme
     */
    public static void getConsumer(String exchangeName ,String queueNAme){
        try {
            // 获取RabbitMq的连接
            ConnectionFactory factory = new ConnectionFactory();

            // 设置ip
            factory.setHost("47.109.83.49");
            // 设置登录的用户名
            factory.setUsername("zcw");
            // 设置登录密码
            factory.setPassword("zcw");
            // 创建连接
            Connection connection = factory.newConnection();
            // 设置信道
            Channel channel = connection.createChannel();
            // 设置交换机
            //channel.exchangeDeclare(exchangeName,"direct");
            // 设置队列
            //channel.queueDeclare(queueNAme,false,true,false,null);


            // 接收消息的回调
            DeliverCallback deliverCallback = (consumerTag, message) -> {
                System.out.println("message:" + new String(message.getBody()));
            };

            // 取消消息的回调
            CancelCallback cancelCallback = consumerTag -> {
                System.out.println("消费者消费消息被中断");
            };


            System.out.println(1231564);

            /**
             *queue：要消费的队列名称
             * autoAck：
             * 如果为 true，则消费者接收消息后立即自动确认（acknowledge）消息，RabbitMQ 将立即将消息从队列中删除，即使后续处理过程中发生错误，消息也不会被重新投递。
             * 如果为 false，则消费者需要手动调用 Channel.basicAck(deliveryTag, false) 来确认消息成功处理，否则消息将在一定重试次数后重新投递给其他消费者或者返回到队列头部。
             * callback：消费者回调接口实现类的对象，当有新消息到达时，RabbitMQ 将调用这个对象的 handleDelivery 方法来传递消息内容及其它相关信息。
             * consumerTag：可选参数，用来标识消费者的唯一标记。如果不提供，RabbitMQ 将自动生成一个唯一的消费者标签。
             */
            channel.basicConsume(queueNAme,true,deliverCallback,cancelCallback);
            //connection.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }





    }


}
