package com.zcw.rabbitmq.hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 生产者类
 * 用于发消息给队列
 */
public class Producer {

//    private static final String QUEUE_NAME = "QUEUE_ONE";


    public static void getConnection(String exchangeName,String QUEUE_NAME){
        try {
            // 创建工厂连接
            ConnectionFactory factory = new ConnectionFactory();
            // 设置连接的IP
            factory.setHost("47.109.83.49");
            //factory.setPort(5672);
            //factory.setVirtualHost("/");
            // 设置连接的用户
            factory.setUsername("zcw");
            // 设置连接的密码
            factory.setPassword("zcw");
            // 创建连接
            Connection connection = factory.newConnection();
            // 获取信道
            Channel channel = connection.createChannel();
            // 刚开始学，使用默认交换机，直接连接队列
            //channel.exchangeDeclare(exchangeName,"direct");

            /**
             * 五个参数
             *  queuenqme， 自己起的队列的名称
             *  durable 消息是否持久化
             *  exclusive 队列中的消息是否共享
             *  autodelete 消息是否自动删除
             *  arguments 其他的参数
             */
            channel.queueDeclare(QUEUE_NAME,false ,false,false,null);
            // 需要发送的消息
            String msg = "first message！";

            /**
             * 发送消息到Rabbitmq需要的参数
             * exchange 发送到哪个交换机
             * routingKey 路由的key，暂定队列名称
             * basicProperties 其他参数
             * byte[] 发送给消息的消息体
             */
            channel.basicPublish("", QUEUE_NAME,null,msg.getBytes());

            for (int i = 0; i < 100; i++) {
                channel.basicPublish("", QUEUE_NAME,null,msg.getBytes());
            }
            System.out.println("消息发送完毕");
            //onnection.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }


    }
}
