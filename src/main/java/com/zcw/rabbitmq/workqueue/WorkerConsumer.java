package com.zcw.rabbitmq.workqueue;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zcw.rabbitmq.util.RabbitConnectionUtil;

import java.io.IOException;

public class WorkerConsumer {

    public static void getMessage(final int consumerName){
        Channel channel = RabbitConnectionUtil.getRabbitConnection();
        String queueName = RabbitConnectionUtil.getQueueName();

        try {
            channel.queueDeclare(queueName, false,false,false,null);

            DeliverCallback deliverCallback = (consumerTag,message) ->{
                System.out.println("消费者" + consumerName + "消费了message: " + new String(message.getBody()));
            };
            CancelCallback cancelCallback = (consumerTage) ->{
                System.out.println("消息取消接收");
            };
            channel.basicConsume(queueName,true,deliverCallback,cancelCallback);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
