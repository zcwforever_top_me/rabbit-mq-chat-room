package com.zcw.rabbitmq.chatroomv2;

import com.zcw.rabbitmq.util.RabbitConnectionUtil;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Test {

    private  static  String ROOM_NAME = "chat_room_1";
    public static void main(String[] args) {

//        startAlternatingExecution(0, 2);
        startAlternatingExecution1(0, 2000);
    }

    public static void startAlternatingExecution1(long delay, long period) {
        Timer timer = new Timer();
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的用户名：");
        String userid = scanner.next();

        // 开始执行方法A
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("请输入你要发送的消息：");
                Sender.sendMessage(ROOM_NAME, userid , userid + ": " + scanner.next());
            }
        }, delay, period * 2);

        // 方法A执行后，延迟一段时间再开始执行方法B
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Subscriber.getMessage(ROOM_NAME, userid);
            }
        }, delay + period, period * 2);

    }


}
