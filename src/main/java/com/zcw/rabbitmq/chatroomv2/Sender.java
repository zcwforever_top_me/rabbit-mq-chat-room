package com.zcw.rabbitmq.chatroomv2;

import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 * 消息发送者
 */
public class Sender {

    public static Channel channel;

    public static void sendMessage(String roomName,String userName, String message){
        // 获取房间
        channel = ChatRoom.getChatRoom(roomName, userName);
        // 发送消息
        try {
            channel.basicPublish(roomName, "", null, message.getBytes("UTF-8"));
            System.out.println("消息：'" + message + "'发送成功！");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }


}
