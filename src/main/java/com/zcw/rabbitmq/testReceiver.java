package com.zcw.rabbitmq;

import com.zcw.rabbitmq.chatroom.ChatRoomPublisher;
import com.zcw.rabbitmq.chatroom.ChatRoomSubscriber;
import com.zcw.rabbitmq.hello.Consumer;
import com.zcw.rabbitmq.hello.Producer;
import com.zcw.rabbitmq.util.RabbitConnectionUtil;
import com.zcw.rabbitmq.workqueue.WorkerConsumer;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class testReceiver {

    private static final String EXCHANGE_NAME = "exchangeOne";
    private static final String QUEUE_NAME = "QUEUE_TOW";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        testSimpleQueue();
//        testWorkQueue();
//        send(scanner);
        getMesage(scanner);
    }

    public static void send(Scanner scanner){
        System.out.println("请输入要发送的消息:");
        while (scanner.hasNext()){
            ChatRoomPublisher.sendMesage(scanner.next());
        }
    }
    public static void getMesage(Scanner scanner){
        System.out.println("请选择要进入的群聊: A 或 B");
        String select = scanner.next();
        String queueName = RabbitConnectionUtil.MEBER_QUEUE_NAME1;
        if (!"A".equals(select)){
            queueName = RabbitConnectionUtil.MEBER_QUEUE_NAME2;
        }
        Thread thread = ChatRoomSubscriber.createConsumer(queueName);
        thread.start();
    }

    public static void testSimpleQueue(){
        Producer.getConnection(EXCHANGE_NAME,QUEUE_NAME);
        Consumer.getConsumer(EXCHANGE_NAME,QUEUE_NAME);
    }

    public static void testWorkQueue(){
//        WorkProducer. getProducer();
        getWorkComsumer();
    }

    public static void getWorkComsumer(){
        ExecutorService service = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 3; i++) {
            int name = i;
            Runnable woker = () ->{
                //int name = finalI;
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                WorkerConsumer.getMessage(name);
            };
            service.execute(woker);
        }


    }


}
