package com.zcw.rabbitmq.chatroomv3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatRoomThead {
    private static ExecutorService service = Executors.newFixedThreadPool(2);

    /**
     * 将消息发送者加入线程中
     */
    public static void addSenderToThread(Sender sender){
        if (sender == null){
            System.out.println("未获取到Sender，无法发送消息");
            return;
        }

        Runnable runnable = () -> {
            while (true){
                sender.sendMessage();
            }
        };
        // 启动线程
        service.execute(runnable);
    }

    /**
     * 将消息接收者加入线程中
     */
    public static void addSubscriberToThread(Subscriber subscriber){
        if (subscriber == null){
            System.out.println("未获取到Subscriber，无法接收消息");
            return;
        }
        Runnable runnable = () -> {
            while (true){
                subscriber.getMessage();
            }
        };
        // 启动线程
        service.execute(runnable);
    }


}
