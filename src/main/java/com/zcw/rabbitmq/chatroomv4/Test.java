package com.zcw.rabbitmq.chatroomv4;

import java.util.Scanner;

/**
 * 扩展聊天室功能
 * 1.支持多线程
 * 2.划分发送消息大小
 * 3.支持自动化测试
 */
public class Test {
    private static String ROOM_NAME = "chat_room_3";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的用户名：");
        String userName = scanner.next();
        Sender sender = new Sender(ROOM_NAME, userName);
        Subscriber subscriber = new Subscriber(ROOM_NAME, userName);
        ChatRoomThead.addSenderToThread(sender);
        ChatRoomThead.addSubscriberToThread(subscriber);

        while (scanner.hasNext()){
            System.out.println("请继续输入要发送的消息：");
            sender.insertMessage(scanner.next());
        }
    }




}
